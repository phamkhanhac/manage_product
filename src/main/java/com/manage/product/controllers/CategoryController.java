package com.manage.product.controllers;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Categories;
import com.manage.product.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(path = "api/v1/categories")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping
    public ResponseEntity<Object> getAllCategories() {

        return categoryService.findAll();
    }

    @GetMapping("/{categoryId}")
    public ResponseEntity<Object> findById(@PathVariable Long categoryId)
            throws BaseException {

        return categoryService.findById(categoryId);
    }

    @PostMapping
    public ResponseEntity<Object> create(@RequestBody @Valid Categories category) throws BaseException {

        return categoryService.create(category);
    }

    @GetMapping("/q")
    public ResponseEntity<Object> find(@RequestParam String name,
                                       @RequestParam Integer pageNumber,
                                       @RequestParam Integer pageSize) throws BaseException {
        Categories categories = new Categories();
        categories.setName(name);

        return categoryService.find(categories, pageNumber, pageSize);
    }

    @PatchMapping("/{categoryId}")
    public ResponseEntity<Object> update(@PathVariable Long categoryId,
                                         @RequestBody Categories category) throws BaseException {

        return categoryService.update(categoryId, category);
    }

    @DeleteMapping("/{categoryId}")
    public ResponseEntity<Object> delete(@PathVariable Long categoryId)
            throws BaseException {

        return categoryService.delete(categoryId);
    }
}
