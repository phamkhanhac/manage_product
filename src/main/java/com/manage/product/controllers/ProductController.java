package com.manage.product.controllers;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Products;
import com.manage.product.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(path = "api/v1/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping
    public ResponseEntity<Object> getAllProducts() {

        return productService.findAll();
    }

    @GetMapping("/{productId}")
    public Products findById(@PathVariable Long productId) throws BaseException {

        return productService.findById(productId);
    }

    @PostMapping
    public ResponseEntity<Object> create(@RequestParam Long categoryId,
                                         @RequestBody @Valid Products product) throws BaseException {

        return productService.create(categoryId, product);
    }

    @PatchMapping("/{productId}")
    public ResponseEntity<Object> update(@PathVariable Long productId,
                                         @RequestBody Optional<Products> product) throws BaseException {
        if (product.isPresent()) {
            return productService.update(productId, product.get());
        }
        else {
            throw new BaseException("COMMON_INPUT_INFO_INVALID");
        }
    }

    @DeleteMapping("/{productId}")
    public ResponseEntity<Object> delete(@PathVariable Long productId) throws BaseException {

        return productService.delete(productId);
    }
}
