package com.manage.product.controllers;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Users;
import com.manage.product.models.dtos.UserDto;
import com.manage.product.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "api/v1/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    public UserController(UserService userService) {
        super();
        this.userService = userService;
    }

    @GetMapping
    public List<UserDto> getAllProducts() {

        return userService.findAll().stream().map(users -> modelMapper.map(users, UserDto.class))
                .collect(Collectors.toList());
    }

    @GetMapping("/{userId}")
    public ResponseEntity<UserDto> findById(@PathVariable Long userId) throws BaseException {
        Users user = userService.findById(userId);
        UserDto userDto = modelMapper.map(user, UserDto.class);

        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UserDto> create(@RequestBody @Valid Users user) throws BaseException {
        userService.create(user);
        UserDto userResponse = modelMapper.map(user, UserDto.class);

        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }

    @PatchMapping("/{userId}")
    public ResponseEntity<UserDto> update(@PathVariable Long userId,
                                         @RequestBody Users user) throws BaseException {
        UserDto userDto = userService.update(userId, user);

        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<UserDto> delete(@PathVariable Long userId) throws BaseException {
        userService.delete(userId);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
