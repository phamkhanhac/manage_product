package com.manage.product.controllers;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Orders;
import com.manage.product.models.dtos.OrderDto;
import com.manage.product.services.OrdersProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "api/v1/playOrder")
public class OrdersProductsController {

    @Autowired
    private OrdersProductsService ordersProductsService;

    @PostMapping
    public ResponseEntity<OrderDto> create(@RequestParam Long userId,
                                           @RequestBody @Valid Orders order) throws BaseException {

        return ordersProductsService.create(userId, order);
    }
}
