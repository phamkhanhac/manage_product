package com.manage.product.controllers;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Orders;
import com.manage.product.models.dtos.OrderDto;
import com.manage.product.models.dtos.OrderDtoList;
import com.manage.product.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;

@RestController
@RequestMapping(path = "api/v1/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping
    public ResponseEntity<OrderDtoList> findById(@RequestParam Long userId) throws BaseException {

        return orderService.findById(userId);
    }

    @PatchMapping("/{orderId}")
    public ResponseEntity<OrderDto> update(@PathVariable Long orderId,
                                           @RequestBody Optional<Orders> order) throws BaseException {
        if (order.isPresent()) {
            return orderService.update(orderId, order.get());
        }
        else {
            throw new BaseException("COMMON_INPUT_INFO_INVALID");
        }
    }
}
