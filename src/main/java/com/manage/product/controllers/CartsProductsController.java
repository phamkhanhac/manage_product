package com.manage.product.controllers;

import com.manage.product.base.handler.BaseException;
import com.manage.product.dto.request.ProductRequest;
import com.manage.product.models.dtos.CartDto;
import com.manage.product.services.CartsProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "api/v1/addToCart")
public class CartsProductsController {

    @Autowired
    private CartsProductsService cartsProductsService;

    @PostMapping
    public ResponseEntity<CartDto> create(@RequestParam Long userId,
                                          @RequestBody @Valid ProductRequest productRequest) throws BaseException {

        return cartsProductsService.create(userId, productRequest);
    }

    @PatchMapping
    public ResponseEntity<CartDto> update(@RequestParam Long userId,
                                          @RequestBody @Valid ProductRequest productRequest) throws BaseException {

        return cartsProductsService.update(userId, productRequest);
    }
}
