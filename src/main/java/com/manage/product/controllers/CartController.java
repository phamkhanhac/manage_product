package com.manage.product.controllers;

import com.manage.product.base.handler.BaseException;
import com.manage.product.models.dtos.CartDto;
import com.manage.product.services.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/v1/carts")
public class CartController {

    @Autowired
    private CartService cartService;

    @GetMapping
    public ResponseEntity<CartDto> findById(@RequestParam Long userId) throws BaseException {

        return cartService.findById(userId);
    }

    @DeleteMapping
    public ResponseEntity<CartDto> delete(@RequestParam Long userId,
                                          @RequestParam Long productId) throws BaseException {
        cartService.delete(userId, productId);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
