package com.manage.product.controllers;

import com.manage.product.base.handler.BaseException;
import com.manage.product.services.FavoriteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/v1/favorites")
public class FavoriteController {

    @Autowired
    private FavoriteService favoriteService;

    @GetMapping("/{favoriteId}")
    public ResponseEntity<Object> findById(@PathVariable Long favoriteId) throws BaseException {

        return favoriteService.findById(favoriteId);
    }

    @PostMapping
    public ResponseEntity<Object> create(@RequestParam Long userId,
                                         @RequestParam Long productId) throws BaseException {

        return favoriteService.create(userId, productId);
    }

    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestParam Long userId, @RequestParam Long productId) throws BaseException {
        favoriteService.delete(userId, productId);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
