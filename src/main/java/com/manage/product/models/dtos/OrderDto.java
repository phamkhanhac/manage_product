package com.manage.product.models.dtos;

import com.manage.product.entity.Orders;
import com.manage.product.entity.OrdersProducts;
import lombok.Data;

import java.util.List;

@Data
public class OrderDto {

    private Orders order;
    private List<OrdersProducts> ordersProducts;

}
