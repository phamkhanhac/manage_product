package com.manage.product.models.dtos;

import com.manage.product.entity.CartsProducts;
import lombok.Data;
import java.math.BigDecimal;
import java.util.List;

@Data
public class CartDto {

    private List<CartsProducts> cartsProducts;
    private BigDecimal totalCost;

}
