package com.manage.product.models.dtos;

import lombok.Data;

@Data
public class UserDto {
    private String name;
    private String age;
    private String email;
    private String address;

    @Override
    public String toString() {
        return "UserDto{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
