package com.manage.product.models.dtos;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductDto {

    private Long productId;
    private int quantity;
    private BigDecimal totalCost;

}
