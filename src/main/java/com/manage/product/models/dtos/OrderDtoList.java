package com.manage.product.models.dtos;

import com.manage.product.entity.Orders;
import lombok.Data;

import java.util.List;

@Data
public class OrderDtoList {

    private List<Orders> order;
}
