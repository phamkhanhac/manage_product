package com.manage.product.respositories;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Orders;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface OrderRepositoryCustom {
    Page<Orders> find(Orders order, Pageable pageable) throws BaseException;
}
