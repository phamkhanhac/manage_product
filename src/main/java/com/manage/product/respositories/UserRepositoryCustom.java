package com.manage.product.respositories;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserRepositoryCustom {
    Page<Users> find(Users user, Pageable pageable) throws BaseException;
}
