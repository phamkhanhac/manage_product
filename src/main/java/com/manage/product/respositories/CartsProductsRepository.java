package com.manage.product.respositories;

import com.manage.product.entity.Carts;
import com.manage.product.entity.CartsProducts;
import com.manage.product.entity.Products;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface CartsProductsRepository extends JpaRepository<CartsProducts, Long> {

    CartsProducts findByProductAndCart(Products product, Carts cart);

    List<CartsProducts> findAllByCart(Carts cart);

}