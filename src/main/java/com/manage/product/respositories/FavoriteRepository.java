package com.manage.product.respositories;

import com.manage.product.entity.Favorites;
import com.manage.product.entity.Products;
import com.manage.product.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FavoriteRepository extends JpaRepository<Favorites, Long>, FavoriteRepositoryCustom {

    Favorites findByUser(Users user);

    Favorites findByUserAndProduct(Users user, Products product);

}
