package com.manage.product.respositories;

import com.manage.product.entity.Orders;
import com.manage.product.entity.OrdersProducts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrdersProductsRepository extends JpaRepository<OrdersProducts, Long> {

    List<OrdersProducts> findAllByOrder(Orders order);
}
