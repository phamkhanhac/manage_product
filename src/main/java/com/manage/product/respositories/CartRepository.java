package com.manage.product.respositories;

import com.manage.product.entity.Carts;
import com.manage.product.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<Carts, Long>, CartRepositoryCustom {

    Carts findByUser(Users user);
}
