package com.manage.product.respositories;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Carts;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CartRepositoryCustom {
    Page<Carts> find(Carts cart, Pageable pageable) throws BaseException;
}
