package com.manage.product.respositories.impl;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Favorites;
import com.manage.product.respositories.FavoriteRepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.Map;

@Repository
public class FavoriteRepositoryCustomImpl implements FavoriteRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Page<Favorites> find(Favorites favorite, Pageable pageable) throws BaseException {
        StringBuilder jpdl = new StringBuilder();
        Map<String, Object> params = new HashMap<>();

        jpdl.append(" select * from favorites where user_id = :user_id ");
        params.put("user_id", favorite.getUser().getId());

        Query countQuery = em.createNativeQuery(" select count(*) from (" + jpdl + ") a");
        params.forEach(countQuery::setParameter);
        int total = ((Number) countQuery.getSingleResult()).intValue();

        jpdl.append(" limit :limit offset: offset ");
        int limit = pageable.getPageSize();
        int offset = (pageable.getPageNumber() - 1) * limit;
        params.put("limit", limit);
        params.put("offset", offset);

        Query query = em.createNativeQuery(jpdl.toString(), Favorites.class);
        params.forEach(query::setParameter);

        Page<Favorites> page = new PageImpl<>(countQuery.getResultList(), pageable, total);

        return page;
    }
}
