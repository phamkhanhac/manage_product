package com.manage.product.respositories.impl;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Products;
import com.manage.product.respositories.ProductRepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.util.HashMap;
import java.util.Map;

@Repository
public class ProductRepositoryCustomImpl implements ProductRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Page<Products> find(Products product, Pageable pageable) throws BaseException {
        StringBuilder jpdl = new StringBuilder();
        Map<String, Object> params = new HashMap<>();

        jpdl.append(" select * from products where name = :name ");
        params.put("name", product.getName());

        Query countQuery = em.createNativeQuery(" select count(*) from (" + jpdl + ") a");
        params.forEach(countQuery::setParameter);
        int total = ((Number) countQuery.getSingleResult()).intValue();

        jpdl.append(" limit :limit offset :offset");
        int limit = pageable.getPageNumber();
        int offset = pageable.getPageSize() - 1;
        params.put("limit", limit);
        params.put("offset", offset);

        Query query = em.createNativeQuery(jpdl.toString(), Products.class);
        params.forEach(query::setParameter);

        Page<Products> page = new PageImpl<>(query.getResultList(), pageable, total);

        return page;
    }
}
