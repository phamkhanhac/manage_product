package com.manage.product.respositories;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Products;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductRepositoryCustom {
    Page<Products> find(Products product, Pageable pageable) throws BaseException;
}
