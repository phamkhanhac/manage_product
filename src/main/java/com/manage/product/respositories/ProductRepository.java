package com.manage.product.respositories;

import com.manage.product.entity.Products;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Products, Long>, ProductRepositoryCustom {

}
