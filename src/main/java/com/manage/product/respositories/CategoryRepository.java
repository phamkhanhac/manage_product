package com.manage.product.respositories;

import com.manage.product.entity.Categories;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CategoryRepository extends JpaRepository<Categories, Long>, CategoryRepositoryCustom {
}
