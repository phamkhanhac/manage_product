package com.manage.product.respositories;

import com.manage.product.entity.Orders;
import com.manage.product.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Orders, Long>, OrderRepositoryCustom {

    List<Orders> findByUser(Users user);
}
