package com.manage.product.respositories;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Categories;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CategoryRepositoryCustom {
    Page<Categories> find(Categories category, Pageable pageable) throws BaseException;
}
