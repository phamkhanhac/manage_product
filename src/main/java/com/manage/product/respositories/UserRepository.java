package com.manage.product.respositories;

import com.manage.product.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<Users, Long>, UserRepositoryCustom {
}
