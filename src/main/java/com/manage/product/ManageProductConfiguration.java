package com.manage.product;

import com.manage.product.base.initialdata.InitialSampleDataPostProcessor;
import com.manage.product.base.initialdata.InitialSampleDataSetup;
import com.manage.product.base.initialdata.SampleData;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class ManageProductConfiguration {

    @Bean
    public InitialSampleDataPostProcessor sampleDataPostProcessor () {
        return new InitialSampleDataPostProcessor();
    }

    @Bean("sampleDataList")
    public List<SampleData> sampleDataList () {
        return new ArrayList<>();
    }

    @Bean
    public InitialSampleDataSetup initialSampleDataSetup(@Qualifier("sampleDataList") List<SampleData> sampleDataList) {
        return new InitialSampleDataSetup(sampleDataList);
    }
}
