package com.manage.product.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Data
@Table(name = "cartsProducts")
public class CartsProducts {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull(message = "Thieu Quantity")
    @Column(name = "quantity")
    private int quantity;

    @Column(name = "total_cost")
    private BigDecimal totalCost;

    @ManyToOne
    @JoinColumn(name = "cart_id")
    @JsonIgnore
    private Carts cart;

    @ManyToOne
    @JoinColumn(name = "product_id")
    @JsonIgnore
    private Products product;

    @Override
    public String toString() {
        return "AddToCart{" +
                "id=" + id +
                ", quantity=" + quantity +
                ", totalCost=" + totalCost +
                ", cart=" + cart +
                ", product=" + product +
                '}';
    }
}
