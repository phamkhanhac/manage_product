package com.manage.product.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Data
@Entity
@Table(name = "users")
public class Users extends Time {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotEmpty(message = "Thieu Name")
    @Column(name = "name")
    private String name;

    @NotEmpty(message = "Thieu Age")
    @Column(name = "age")
    private String age;

    @Email(message = "Email Khong Hop Le")
    @Column(name = "email")
    private String email;

    @NotEmpty(message = "Thieu Address")
    @Column(name = "address")
    private String address;

    @NotEmpty(message = "Thieu Role")
    @Column(name = "role")
    private String role;

    @NotEmpty(message = "Thieu UserName")
    @Column(name = "username")
    private String username;

    @NotEmpty(message = "Thieu Password")
    @Column(name = "password")
    private String password;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    @JsonManagedReference
    @JsonIgnore
    private Set<Orders> order;

    @Override
    public String toString() {
        return "Users{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", role='" + role + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", order=" + order +
                '}';
    }
}
