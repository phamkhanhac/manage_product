package com.manage.product.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.util.Set;

@Data
@Entity
@Table(name = "orders")
public class Orders extends Time {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotEmpty(message = "Thieu Name")
    @Column(name = "name")
    private String name;

    @Column(name = "total_cost")
    private BigDecimal totalCost;

//    @GeneratedValue(generator = "sequence-generator")
//    @GenericGenerator(
//            name = "sequence-generator",
//            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
//            parameters = {
//                    @Parameter(name = "sequence_name", value = "user_sequence"),
//                    @Parameter(name = "initial_value", value = "O0001"),
//                    @Parameter(name = "increment_size", value = "1")
//            }
//    )

    @Column(name = "code")
    private int code;

    @Column(name = "status")
    private String status;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonBackReference
    private Users user;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "order_id")
    @JsonManagedReference
    @JsonIgnore
    private Set<OrdersProducts> ordersProducts;

    @Override
    public String toString() {
        return "Orders{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", totalCost=" + totalCost +
                ", code=" + code +
                ", status='" + status + '\'' +
                ", user=" + user +
                ", playOrder=" + ordersProducts +
                '}';
    }
}
