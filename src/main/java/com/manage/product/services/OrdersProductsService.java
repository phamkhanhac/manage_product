package com.manage.product.services;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Orders;
import com.manage.product.models.dtos.OrderDto;
import org.springframework.http.ResponseEntity;

public interface OrdersProductsService {

    ResponseEntity<OrderDto> create(Long cartId, Orders order) throws BaseException;

}
