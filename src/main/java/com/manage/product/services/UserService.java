package com.manage.product.services;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Users;
import com.manage.product.models.dtos.UserDto;

import java.util.List;

public interface UserService {

    List<Users> findAll() throws BaseException;

    Users findById(Long userId) throws BaseException;

    Users create(Users user) throws BaseException;

    UserDto update(Long userId, Users user) throws BaseException;

    void delete(Long userId) throws BaseException;

}
