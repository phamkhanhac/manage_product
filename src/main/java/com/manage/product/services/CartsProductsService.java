package com.manage.product.services;

import com.manage.product.base.handler.BaseException;
import com.manage.product.dto.request.ProductRequest;
import com.manage.product.entity.CartsProducts;
import com.manage.product.models.dtos.CartDto;
import org.springframework.http.ResponseEntity;

public interface CartsProductsService {

    ResponseEntity<CartDto> create(Long userId, ProductRequest productRequest) throws BaseException;

    ResponseEntity<CartDto> update(Long userId, ProductRequest productRequest) throws BaseException;

}
