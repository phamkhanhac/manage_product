package com.manage.product.services;

import com.manage.product.base.handler.BaseException;
import org.springframework.http.ResponseEntity;

public interface FavoriteService {

    ResponseEntity<Object> findById(Long favoriteId) throws BaseException;

    ResponseEntity<Object> create(Long userId, Long productId) throws BaseException;

    ResponseEntity<Object> delete(Long userId, Long productId) throws BaseException;

}
