package com.manage.product.services;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Orders;
import com.manage.product.models.dtos.OrderDto;
import com.manage.product.models.dtos.OrderDtoList;
import org.springframework.http.ResponseEntity;

public interface OrderService {

    ResponseEntity<OrderDtoList> findById(Long userId) throws BaseException;

    ResponseEntity<OrderDto> update(Long orderId, Orders order) throws BaseException;

}
