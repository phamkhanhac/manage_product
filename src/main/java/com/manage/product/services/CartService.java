package com.manage.product.services;

import com.manage.product.base.handler.BaseException;
import com.manage.product.models.dtos.CartDto;
import org.springframework.http.ResponseEntity;

public interface CartService {

    ResponseEntity<CartDto> findById(Long userId) throws BaseException;

    void delete(Long userId, Long productId) throws BaseException;

}
