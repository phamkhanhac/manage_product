package com.manage.product.services;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Products;
import org.springframework.http.ResponseEntity;

public interface ProductService {

    ResponseEntity<Object> findAll() throws BaseException;

    Products findById(Long productId) throws BaseException;

    ResponseEntity<Object> create(Long categoryId, Products product) throws BaseException;

    ResponseEntity<Object> update(Long productId, Products product) throws BaseException;

    ResponseEntity<Object> delete(Long productId) throws BaseException;

}
