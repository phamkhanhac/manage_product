package com.manage.product.services.impl;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Favorites;
import com.manage.product.entity.Products;
import com.manage.product.entity.Users;
import com.manage.product.respositories.FavoriteRepository;
import com.manage.product.services.FavoriteService;
import com.manage.product.services.ProductService;
import com.manage.product.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class FavoriteServiceImpl implements FavoriteService {

    private static final String NOT_EXIST = "favorite.not.exist";

    @Autowired
    private FavoriteRepository favoriteRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    @Override
    public ResponseEntity<Object> findById(Long userId) throws BaseException {
        Users oUser = userService.findById(userId);

        return new ResponseEntity<>(favoriteRepository.findByUser(oUser), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> create(Long userId, Long productId) throws BaseException {
        Users oUser = userService.findById(userId);
        Products oProduct = productService.findById(productId);

        Favorites oFavorite = favoriteRepository.findByUserAndProduct(oUser, oProduct);

        if (oFavorite == null) {
            Favorites favorite = new Favorites();
            favorite.setUser(oUser);
            favorite.setProduct(oProduct);

            favoriteRepository.save(favorite);

            return new ResponseEntity<>(favorite, HttpStatus.OK);
        }
        else {
            throw new BaseException(NOT_EXIST);
        }
    }

    @Override
    public ResponseEntity<Object> delete(Long userId, Long productId) throws BaseException {
        Users oUser = userService.findById(userId);
        Products oProduct = productService.findById(productId);

        Favorites oFavorite = favoriteRepository.findByUserAndProduct(oUser, oProduct);

        if (oFavorite == null) {
            throw new BaseException(NOT_EXIST);
        }

        favoriteRepository.delete(oFavorite);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
