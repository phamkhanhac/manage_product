package com.manage.product.services.impl;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.*;
import com.manage.product.models.dtos.OrderDto;
import com.manage.product.respositories.*;
import com.manage.product.services.OrdersProductsService;
import com.manage.product.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class OrdersProductsServiceImpl implements OrdersProductsService {

    private static final String NOT_EXIST = "ordersProducts.not.exist";

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrdersProductsRepository ordersProductsRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private CartsProductsRepository cartsProductsRepository;

    @Override
    public ResponseEntity<OrderDto> create(Long userId, Orders order) throws BaseException {
        Users oUser = userService.findById(userId);
        Carts oCart = cartRepository.findByUser(oUser);

        if (oCart == null) {
            throw new BaseException(NOT_EXIST);
        }

        order.setUser(oUser);
        order.setTotalCost(oCart.getTotalCost());
        orderRepository.save(order);
        
        List<CartsProducts> cartsProductsList = cartsProductsRepository.findAllByCart(oCart);

        for (int i = 0; i < cartsProductsList.size(); i++) {
            OrdersProducts ordersProducts = new OrdersProducts();

            ordersProducts.setOrder(order);
            ordersProducts.setProduct(cartsProductsList.get(i).getProduct());
            ordersProducts.setQuantity(cartsProductsList.get(i).getQuantity());
            ordersProducts.setTotalCost(cartsProductsList.get(i).getTotalCost());

            ordersProductsRepository.save(ordersProducts);
        }

        cartsProductsRepository.deleteAll(cartsProductsList);
        cartRepository.delete(oCart);

        List<OrdersProducts> ordersProductsList = ordersProductsRepository.findAllByOrder(order);

        OrderDto orderDto = new OrderDto();
        orderDto.setOrder(order);
        orderDto.setOrdersProducts(ordersProductsList);

        return new ResponseEntity<>(orderDto, HttpStatus.OK);
    }
}
