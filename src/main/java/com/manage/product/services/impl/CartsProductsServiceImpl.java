package com.manage.product.services.impl;

import com.manage.product.base.handler.BaseException;
import com.manage.product.dto.request.ProductRequest;
import com.manage.product.entity.Carts;
import com.manage.product.entity.CartsProducts;
import com.manage.product.entity.Products;
import com.manage.product.entity.Users;
import com.manage.product.models.dtos.CartDto;
import com.manage.product.respositories.CartRepository;
import com.manage.product.respositories.CartsProductsRepository;
import com.manage.product.respositories.ProductRepository;
import com.manage.product.services.CartsProductsService;
import com.manage.product.services.ProductService;
import com.manage.product.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class CartsProductsServiceImpl implements CartsProductsService {

    private static final String NOT_EXIST = "addToCart.not.exist";

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductService productService;

    @Autowired
    private CartsProductsRepository cartsProductsRepository;

    @Autowired
    private UserService userService;

    @Override
    public ResponseEntity<CartDto> create(Long userId, ProductRequest productRequest) throws BaseException {
        if (productRequest.getProduct() == null) {
            throw new BaseException(NOT_EXIST);
        }

        for (int i = 0; i < productRequest.getProduct().size(); i++) {
            Products oProduct = productService.findById(productRequest.getProduct().get(i).getId());

            if (productRequest.getProduct().get(i).getQuantity() < 0 || productRequest.getProduct().get(i).getQuantity() > oProduct.getQuantity()) {
                throw new BaseException(NOT_EXIST);
            }
        }

        Users oUser = userService.findById(userId);
        Carts oCart = cartRepository.findByUser(oUser);

        if (oCart == null) {
            Carts cart = new Carts();

            cart.setUser(oUser);

            for (int i = 0; i < productRequest.getProduct().size(); i++) {
                CartsProducts cartsProducts = new CartsProducts();

                Products product = productService.findById(productRequest.getProduct().get(i).getId());

                BigDecimal quantity = new BigDecimal(productRequest.getProduct().get(i).getQuantity());
                BigDecimal totalCost = quantity.multiply(product.getCost());

                cartsProducts.setProduct(product);
                cartsProducts.setQuantity(productRequest.getProduct().get(i).getQuantity());
                cartsProducts.setTotalCost(totalCost);

                if (i == 0) {
                    cart.setTotalCost(totalCost);
                }
                else {
                    cart.setTotalCost(cart.getTotalCost().add(totalCost));
                }
                cartRepository.save(cart);

                cartsProducts.setCart(cart);
                cartsProductsRepository.save(cartsProducts);
            }
        }
        else {
            for (int i = 0; i < productRequest.getProduct().size(); i++) {
                CartsProducts cartsProducts = new CartsProducts();

                cartsProducts.setCart(oCart);

                Products oProduct = productService.findById(productRequest.getProduct().get(i).getId());

                BigDecimal quantity = new BigDecimal(productRequest.getProduct().get(i).getQuantity());
                BigDecimal totalCost = quantity.multiply(oProduct.getCost());

                oCart.setTotalCost(oCart.getTotalCost().add(totalCost));
                cartRepository.save(oCart);

                CartsProducts oCartsProducts = cartsProductsRepository.findByProductAndCart(oProduct, oCart);

                if (oCartsProducts == null) {
                    cartsProducts.setProduct(oProduct);
                    cartsProducts.setQuantity(productRequest.getProduct().get(i).getQuantity());
                    cartsProducts.setTotalCost(totalCost);

                    cartsProductsRepository.save(cartsProducts);
                }
                else {
                    oCartsProducts.setQuantity(oCartsProducts.getQuantity() + productRequest.getProduct().get(i).getQuantity());
                    oCartsProducts.setTotalCost(oCartsProducts.getTotalCost().add(totalCost));

                    cartsProductsRepository.save(oCartsProducts);
                }
            }
        }
        Carts cartCurrent = cartRepository.findByUser(oUser);
        List<CartsProducts> oCartsProducts = cartsProductsRepository.findAllByCart(cartCurrent);

        CartDto cartDto = new CartDto();
        cartDto.setCartsProducts(oCartsProducts);
        cartDto.setTotalCost(cartCurrent.getTotalCost());

        return new ResponseEntity<>(cartDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CartDto> update(Long userId, ProductRequest productRequest) throws BaseException {
        Users oUser = userService.findById(userId);

        Carts oCart = cartRepository.findByUser(oUser);
        if (oCart == null) {
            throw new BaseException(NOT_EXIST);
        }

        for (int i = 0; i < productRequest.getProduct().size(); i++) {
            Optional<Products> oProduct = productRepository.findById(productRequest.getProduct().get(i).getId() == null ? 0 : productRequest.getProduct().get(i).getId());
            if (!oProduct.isPresent()) {
                throw new BaseException(NOT_EXIST);
            }
            Products product = oProduct.get();

            CartsProducts oCartsProducts = cartsProductsRepository.findByProductAndCart(product, oCart);
            oCartsProducts.setQuantity(productRequest.getProduct().get(i).getQuantity());

            BigDecimal quantity = new BigDecimal(productRequest.getProduct().get(i).getQuantity());
            BigDecimal totalCost = quantity.multiply(oCartsProducts.getProduct().getCost());

            oCart.setTotalCost((oCart.getTotalCost().subtract(oCartsProducts.getTotalCost())).add(totalCost));
            cartRepository.save(oCart);

            oCartsProducts.setTotalCost(totalCost);
            oCartsProducts.setCart(oCart);
            cartsProductsRepository.save(oCartsProducts);
        }

        List<CartsProducts> cartsProductsList = cartsProductsRepository.findAllByCart(oCart);

        CartDto cartDto = new CartDto();
        cartDto.setCartsProducts(cartsProductsList);
        cartDto.setTotalCost(oCart.getTotalCost());

        return new ResponseEntity<>(cartDto, HttpStatus.OK);
    }
}
