package com.manage.product.services.impl;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Categories;
import com.manage.product.respositories.CategoryRepository;
import com.manage.product.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

    private static final String NOT_EXISTS = "category.not.exist";

    @Autowired
    private CategoryRepository categoryRepository;


    @Override
    public ResponseEntity<Object> findAll() throws BaseException {
        return new ResponseEntity<>(categoryRepository.findAll(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> find(Categories category, Integer pageNumber, Integer pageSize)
            throws BaseException {

        return new ResponseEntity<>(categoryRepository.find(category,
                PageRequest.of(pageNumber, pageSize)), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findById(Long categoryId) throws BaseException {
        Optional<Categories> oCategory = categoryRepository.findById(categoryId == null ? 0 : categoryId);
        if (!oCategory.isPresent()) {
            throw new BaseException(NOT_EXISTS);
        }

        return new ResponseEntity<>(oCategory.get(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> create(Categories category) throws BaseException {
        Categories categoryCurrent = categoryRepository.save(category);

        return new ResponseEntity<>(categoryCurrent, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> update(Long categoryId, Categories category) throws BaseException {
        Optional<Categories> oCategory = categoryRepository.findById(categoryId == null ? 0 : categoryId);
        if (!oCategory.isPresent()) {
            throw new BaseException(NOT_EXISTS);
        }
        Categories categoryCurrent = oCategory.get();

        categoryCurrent.setName(category.getName());
        categoryCurrent.setStatus(category.getStatus());
        categoryRepository.save(categoryCurrent);

        return new ResponseEntity<>(categoryCurrent, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> delete(Long categoryId) throws BaseException {
        categoryRepository.deleteById(categoryId);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
