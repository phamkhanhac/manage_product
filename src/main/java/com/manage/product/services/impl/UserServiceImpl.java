package com.manage.product.services.impl;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Users;
import com.manage.product.models.dtos.UserDto;
import com.manage.product.respositories.UserRepository;
import com.manage.product.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private static final String NOT_EXISTS = "product.not.exist";

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<Users> findAll() throws BaseException {
        return userRepository.findAll();
    }

    @Override
    public Users findById(Long userId) throws BaseException {
        Optional<Users> oUser = userRepository.findById(userId == null ? 0 : userId);
        if (!oUser.isPresent()) {
            throw new BaseException(NOT_EXISTS);
        }
        return oUser.get();
    }

    @Override
    public Users create(Users user) throws BaseException {

        return userRepository.save(user);
    }

    @Override
    public UserDto update(Long userId, Users user) throws BaseException {
        Optional<Users> oUser = userRepository.findById(userId == null ? 0 : userId);
        if (!oUser.isPresent()) {
            throw new BaseException(NOT_EXISTS);
        }
        Users userCurrent = oUser.get();

        userCurrent.setName(user.getName());
        userCurrent.setAge(user.getAge());
        userCurrent.setEmail(user.getEmail());
        userCurrent.setAddress(user.getAddress());
        userCurrent.setRole(user.getRole());
        userCurrent.setUsername(user.getUsername());
        userCurrent.setPassword(user.getPassword());

        userRepository.save(userCurrent);

        UserDto userDto = new UserDto();
        userDto.setName(userCurrent.getName());
        userDto.setAddress(userCurrent.getAddress());
        userDto.setAge(userCurrent.getAge());
        userDto.setEmail(userCurrent.getEmail());

        return userDto;
    }

    @Override
    public void delete(Long userId) throws BaseException {

        userRepository.deleteById(userId);
    }
}
