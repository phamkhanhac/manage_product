package com.manage.product.services.impl;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Carts;
import com.manage.product.entity.CartsProducts;
import com.manage.product.entity.Products;
import com.manage.product.entity.Users;
import com.manage.product.models.dtos.CartDto;
import com.manage.product.respositories.CartRepository;
import com.manage.product.respositories.CartsProductsRepository;
import com.manage.product.services.CartService;
import com.manage.product.services.ProductService;
import com.manage.product.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CartServiceImpl implements CartService {

    private static final String NOT_EXISTS = "cart.not.exist";

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private CartsProductsRepository cartsProductsRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    @Override
    public ResponseEntity<CartDto> findById(Long userId) throws BaseException {
        Users oUser = userService.findById(userId);
        Carts oCart = cartRepository.findByUser(oUser);

        if (oCart == null) {
            throw new BaseException(NOT_EXISTS);
        }

        List<CartsProducts> cartsProductsList = cartsProductsRepository.findAllByCart(oCart);

        CartDto cartDto = new CartDto();
        cartDto.setTotalCost(oCart.getTotalCost());
        cartDto.setCartsProducts(cartsProductsList);

        return new ResponseEntity<>(cartDto, HttpStatus.OK);
    }

    @Override
    public void delete(Long userId, Long productId) throws BaseException {
        Users oUser = userService.findById(userId);

        Carts oCart = cartRepository.findByUser(oUser);
        if (oCart == null) {
            throw new BaseException(NOT_EXISTS);
        }

        Products product = productService.findById(productId);
        CartsProducts cartsProducts = cartsProductsRepository.findByProductAndCart(product, oCart);

        oCart.setTotalCost(oCart.getTotalCost().subtract(cartsProducts.getTotalCost()));

        cartsProductsRepository.deleteById(cartsProducts.getId());
        cartRepository.save(oCart);
    }
}
