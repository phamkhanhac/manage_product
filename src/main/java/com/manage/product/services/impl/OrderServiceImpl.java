package com.manage.product.services.impl;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Orders;
import com.manage.product.entity.Users;
import com.manage.product.models.dtos.OrderDto;
import com.manage.product.models.dtos.OrderDtoList;
import com.manage.product.respositories.OrderRepository;
import com.manage.product.services.OrderService;
import com.manage.product.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private static final String NOT_EXISTS = "order.not.exist";

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserService userService;

    @Override
    public ResponseEntity<OrderDtoList> findById(Long userId) throws BaseException {
        Users oUser = userService.findById(userId);
        List<Orders> ordersList = orderRepository.findByUser(oUser);

        if (ordersList == null) {
            throw new BaseException(NOT_EXISTS);
        }

        OrderDtoList orderDtoList = new OrderDtoList();
        orderDtoList.setOrder(ordersList);

        return new ResponseEntity<>(orderDtoList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<OrderDto> update(Long userId, Orders order) throws BaseException {
        Users oUser = userService.findById(userId);
//        Orders orderCurrent = orderRepository.findByUser(oUser);
//
//        if (orderCurrent == null) {
//            throw new BaseException(NOT_EXISTS);
//        }

//        orderCurrent.setCode(order.getCode());
//        orderCurrent.setStatus(order.getStatus());
//
//        orderRepository.save(orderCurrent);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
