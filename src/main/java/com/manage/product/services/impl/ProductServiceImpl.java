package com.manage.product.services.impl;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Categories;
import com.manage.product.entity.Products;
import com.manage.product.respositories.CategoryRepository;
import com.manage.product.respositories.ProductRepository;
import com.manage.product.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    private static final String NOT_EXISTS = "product.not.exist";

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public ResponseEntity<Object> findAll() throws BaseException {
        return new ResponseEntity<>(productRepository.findAll(), HttpStatus.OK);
    }

    @Override
    public Products findById(Long productId) throws BaseException {
        Optional<Products> oProduct = productRepository.findById(productId == null ? 0 : productId);
        if (!oProduct.isPresent()) {
            throw new BaseException(NOT_EXISTS);
        }

        return oProduct.get();
    }

    @Override
    public ResponseEntity<Object> create(Long categoryId, Products product) throws BaseException {
        Optional<Categories> oCategory = categoryRepository.findById(categoryId == null ? 0 : categoryId);
        if (!oCategory.isPresent()) {
            throw new BaseException(NOT_EXISTS);
        }
        Categories category = oCategory.get();

        product.setCategory(category);
        productRepository.save(product);

        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> update(Long productId, Products product) throws BaseException {
        Optional<Products> oProduct = productRepository.findById(productId == null ? 0 : productId);
        if (!oProduct.isPresent()) {
            throw new BaseException(NOT_EXISTS);
        }
        Products productCurrent = oProduct.get();

        productCurrent.setName(product.getName());
        productCurrent.setQuantity(product.getQuantity());
        productCurrent.setCost(product.getCost());

        productRepository.save(productCurrent);

        return new ResponseEntity<>(productCurrent, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> delete(Long productId) throws BaseException {
        productRepository.deleteById(productId);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
