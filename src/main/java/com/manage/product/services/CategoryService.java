package com.manage.product.services;

import com.manage.product.base.handler.BaseException;
import com.manage.product.entity.Categories;

import org.springframework.http.ResponseEntity;

public interface CategoryService {

    ResponseEntity<Object> findAll() throws BaseException;

    ResponseEntity<Object> find(Categories category, Integer pageNumber, Integer pageSize) throws BaseException;

    ResponseEntity<Object> findById(Long categoryId) throws BaseException;

    ResponseEntity<Object> create(Categories category) throws BaseException;

    ResponseEntity<Object> update(Long categoryId, Categories category) throws BaseException;

    ResponseEntity<Object> delete(Long categoryId) throws BaseException;

}
