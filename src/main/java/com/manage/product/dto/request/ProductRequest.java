package com.manage.product.dto.request;

import com.manage.product.entity.Products;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class ProductRequest {

    @NotNull(message = "Thieu List")
    private List<Products> product;
}
