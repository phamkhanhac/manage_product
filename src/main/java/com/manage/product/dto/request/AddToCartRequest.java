package com.manage.product.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AddToCartRequest {

    @NotNull(message = "Thieu product_id")
    private Long productId;

    @NotNull(message = "Thieu quantity")
    private int quantity;

}
